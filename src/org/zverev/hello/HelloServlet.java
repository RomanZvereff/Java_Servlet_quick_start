package org.zverev.hello;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/HelloServlet")
public class HelloServlet extends GenericServlet implements Servlet {
	
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#service(ServletRequest request, ServletResponse response)
	 */
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();
		printWriter.println("<!DOCTYPE html>\n" +
				"<html>\n" + 
				"<head>" +
				"<meta charset='UTF-8'>\n" +
				"<title>Hello Servlet</title>\n" +
				"</head>" + 
				"<body>\n" + 
				"<h2>Hello Servlet</h2>\n" +
				"Go <a href='" + 
					request.getServletContext().getContextPath() + 
				"'>Back</a>\n" + 
				"</body>\n" +
				"</html>");
		printWriter.close();
	}

}
