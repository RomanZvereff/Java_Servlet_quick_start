<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Guess Number Game</title>
<%
	int guess = -1, number, counter;
	String message = "You have to guess number <br/>from 1 up 10 using 3 attempts.";
	String guessStr = request.getParameter("guess");
	String numberStr = request.getParameter("number");
	String guessNumber = "*";
	String counterStr = request.getParameter("counter");
	String backPath = request.getContextPath();
	String selfPath = backPath + request.getServletPath();
%>
</head>
<body>
	<h2>Guess Number Game</h2>
	<%
		if (guessStr == null || guessStr.length() == 0) { // to start game
			number = (int) Math.floor(Math.random()*10 + 1);
			counter = 3;
		} else {
			// next attempt
			guess = Integer.parseInt(guessStr);
			number = Integer.parseInt(numberStr);
			counter = Integer.parseInt(counterStr);
			counter--;
			if (guess == number) {
				message = "Congrats! You guessed, it was " + number + "! You can try to play again.";
				guessNumber = Integer.toString(number);
			}
			else {
				message = "Conceived number is " + ((guess < number)? "greater." : "less.");
				if (counter == 0) {
					message += "<br/>Sorry, You lost. You can try to play again.";
					guessNumber = Integer.toString(number);
				}
			}
			if (counter == 0 || guess == number) {
				number = (int) Math.floor(Math.random()*10 + 1);
			}
		}
	%>
	<h3 style="color:red"><%= message %></h3>
	<form method="POST" action="<%= selfPath %>">
		<input type="hidden" name="counter" value="<%= counter %>"/>
		<input type="hidden" name="number" value="<%= number %>"/>
		<input type="text" name="guess"/>
		<input type="submit" value="Send"/>
	</form>
	<p style="color:grey">Number of attempts: <%= counter %></p>
	<p style="color:grey">Guess number: <%= guessNumber %></p>
	<a href="guess_game.jsp">Play again</a><br>
	Go <a href="<%=request.getServletContext().getContextPath()%>">Back</a>
</body>
</html>